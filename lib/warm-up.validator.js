const { success } = require('./response.wrap');

module.exports = function warmUpValidator(event, callback) {
  if (event.source === 'serverless-plugin-warmup') {
    this.log.info('Warm-up lambda, returning success.');
    callback(null, success());
    return true;
  }
};
