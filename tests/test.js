const test = require('ava');
const { divideEqual, divideEqualtArr } = require('../lib/divide.equal');

const MAX_ARRAY_SIZE = 2;

console.log(divideEqualtArr(['t','e','s','t'],MAX_ARRAY_SIZE));

test('divideEqual split even', t => {
	t.is(divideEqual("test",2)[0],'te');
    t.is(divideEqual("test",2)[1],'st');
});
test('divideEqual split odd', t => {
	t.is(divideEqual("tests",2)[0],'tes');
    t.is(divideEqual("tests",2)[1],'ts');
});
test('divideEqual split empty string', t => {
	t.is(divideEqual("",2)[0],undefined);
});

test('divideEqualtArr even length array', t =>{
    t.deepEqual(divideEqualtArr(['t','e','s','t'],MAX_ARRAY_SIZE),[['t','e'],['s','t']]);
});
test('divideEqualtArr odd length array', t =>{
    t.deepEqual(divideEqualtArr(['t','e','s','t','s'],MAX_ARRAY_SIZE),[['t','e'],['s','t'],['s']]);
});