const responseWrap = require('./response.wrap');
const isItWarmUp = require('./warm-up.validator');
const logger = require('./logger.base');
const divideEqual = require('./divide.equal');

module.exports = {
  responseWrap,
  logger,
  isItWarmUp,
  divideEqual,
};
