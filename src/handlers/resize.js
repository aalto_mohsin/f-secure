'use strict';

const errorInternalServer500 = require('../error_responses/database_connection');
const errorInvalidParameter400 = require('../error_responses/invalid_parameter');
const responseMessage = require('../error_responses/response_messages');
const log = require('../../lib/logger.base')({ level: process.env.log_level });
const { success, failure } = require('../../lib/response.wrap');
const { divideEqual, divideEqualtArr } = require('../../lib/divide.equal');

const MAX_BYTE_SIZE = 1000000;
const MAX_ARRAY_SIZE = 500;

module.exports.handler =  async (event, context, callback) => {
  log.setChild(context);
  try {
    if (!event.body) {
      log.info('Info:', responseMessage.bodyJsonMissing);
          callback(null, failure(400, errorInvalidParameter400(responseMessage.bodyJsonMissing)));
    }
    let bodyJson = JSON.parse(event.body); 
    let data = bodyJson.request;
    var dataSplits = []; 
    const response = {};

    // Split each input
    data.forEach((item)=>{
      let n_splits = Math.ceil(Buffer.byteLength(item, 'utf8') / MAX_BYTE_SIZE);
      if(n_splits > 1) {
        dataSplits = dataSplits.concat(divideEqual(item,n_splits))  
      }
      else {
        dataSplits.push(item);
      }
    });
    
    // Split batches
    let output = divideEqualtArr(dataSplits, MAX_ARRAY_SIZE);
    output.forEach((item, index)=>{
      response[`batch-${index}`] = item;
    });

    log.info('Info:', response);
    return success(response)

  } catch (err) {
    log.error(err, null);
    callback(null, failure(500, errorInternalServer500(err.message)));
  }
  
}