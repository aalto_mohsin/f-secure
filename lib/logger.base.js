const pino = require('pino');

/**
 * @example - Usage in AWS Lambda
 *
 * const log = require('../logger.base')({level: 'info'})
 *
 * module.exports = (event, context, callback) {
 *  log.setChild(context)
 *
 *  log.info('Lambda started')
 * }
 *
 *
 * @env LOG_LEVEL - specifies log level. Could be done via {opts.level}
 * @env LOG_NAME - specifies log name.
 */

function transformMeta(meta) {
  return {
    group: meta.logGroupName,
    stream: meta.logStreamName,
    fn: meta.functionName,
    fnVersion: meta.functionVersion,
    reqId: meta.awsRequestId
  };
}

function wrap(logger) {
  return (options) => {
    const levelMethods = ['debug', 'info', 'warn', 'error'];
    const opts = Object.assign({}, options);
    const proto = {};

    levelMethods.forEach((level) => {
      Object.defineProperty(proto, level, {
        value: function value(...params) {
          this.executor[level](...params);
        }
      });
    });

    // configure default options
    opts.level = opts.level || process.env.LOG_LEVEL || 'info';
    opts.name = `${process.env.LOG_NAME || 'log'}.${opts.level}`;
    opts.timestamp = opts.timestamp || pino.stdTimeFunctions.epochTime;

    Object.defineProperty(proto, 'setChild', {
      value: function setChild(child) {
        this.executor = logger(opts).child(transformMeta(child));
      },
      enumerable: true
    });

    return Object.create(proto, {
      executor: {
        value: logger(opts),
        enumerable: true,
        writable: true
      }
    });
  };
}

// configure here wrapped logger
module.exports = wrap(pino);
