function divideEqual(str, num) {
    const len = str.length / num;
    const creds = str.split("").reduce((acc, val) => {
       let { res, currInd } = acc;
       if(!res[currInd] || res[currInd].length < len){
          res[currInd] = (res[currInd] || "") + val;
       }else{
          res[++currInd] = val;
       };
       return { res, currInd };
    }, {
       res: [],
       currInd: 0
    });
    return creds.res;
};
function divideEqualtArr(arr, num) {
    const result = new Array(Math.ceil(arr.length / num))
    .fill()
    .map(_ => arr.splice(0, num))
    return result;
}

module.exports = { divideEqual, divideEqualtArr };