# F-Secure Homework Assignment
## Pre-requisites
Following tools are required to get thing started for local and AWS deployment:
1. NodeJS
2. Yarn package manager
3. Postman

## Installation
Run following commands to get the API up and running:
### Installing Nodejs
NodeJS can be downloaded from [Node.js](https://nodejs.org/)
### Installing Yarn
Following command can be used to install yarn globally
```sh
npm install --global yarn
```
### Resolve dependencies
```sh
yarn
```
### Deploy API locally
```sh
yarn deploy-local
```
## Calling API
Postman collection contains the request parameter for testing the api

## Deploying on AWS
### Installing AWS CLI
AWS CLI can be downloaded from [AWS CLI](https://aws.amazon.com/cli/)
### Configuring CLI
AWS CLI can be configured using following command:
```sh
aws configure --profile fsecure
```
Please Note: After this command you maybe asked to provide aws_access_key and aws_secret_key.
### Deploying
Deployment can be done simply by running:
```sh
yarn deploy
```
## Unit Test
Unit test are only written for libraries that perform the transformation from INPUT to OUTPUT and can be run and configure in CI/CD using:
```sh
yarn test
```