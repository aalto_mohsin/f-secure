const errorInternalServer500 = function (err) {
  const response = {
    reason: 'data_connection',
    errors: [
      {
        code: 500,
        title: 'Unexpected internal server error occurred',
        detail: err,
        source: {
          pointer: 'db/connection'
        }
      }
    ]
  };
  return response;
};

module.exports = errorInternalServer500;
