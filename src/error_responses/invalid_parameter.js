const errorInvalidParameter400 = function (err) {
  const response = {
    reason: 'invalid_parameter',
    errors: [
      {
        code: 400,
        title: 'Invalid query parameters',
        detail: err,
        source: {
          pointer: 'string'
        }
      }
    ]
  };
  return response;
};

module.exports = errorInvalidParameter400;
