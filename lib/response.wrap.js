function wrapHeaders(obj, customHeadersMap = {}) {
  const resp = Object.assign({}, obj);

  resp.headers = Object.assign(customHeadersMap, {
    'Access-Control-Allow-Origin': '*', // Required for CORS support to work
    'Access-Control-Allow-Credentials': true // Required for cookies, authorization headers with HTTPS
  });

  return resp;
}

function buildResp(code, data) {
  return wrapHeaders({
    statusCode: code,
    body: typeof data === 'object' ? JSON.stringify(data) : data
  });
}

function buildRespWithAuthContextInHeaders(code, data, event = null) {
  let customHeadersMap = null;

  // populate header by authorizer parameters
  if (event && event.requestContext && event.requestContext.authorizer) {
    const authorizer = event.requestContext.authorizer;
    customHeadersMap = {
      'x-resident-world-user-id': authorizer.residentWorldUserId,
      'x-resident-world-user-role': authorizer.residentWorldRole,
      'x-resident-world-cohort-id': authorizer.residentWorldCohortId,
      'x-resident-world-app-id': authorizer.residentWorldAppId
    };
  }

  return wrapHeaders({
    statusCode: code,
    body: typeof data === 'object' ? JSON.stringify(data) : data
  }, customHeadersMap);
}

module.exports = {
  successWithAuthContextInHeaders: (data, event = null) => buildRespWithAuthContextInHeaders(200, data || {}, event),
  success: data => buildResp(200, data || {}),
  failure: (code, err) => buildResp(code, err),
};
