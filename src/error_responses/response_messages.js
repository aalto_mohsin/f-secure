const responseMessages = {
  bodyJsonMissing: 'Invalid Parameter: bodyJson property missing!',
};

module.exports = responseMessages;
